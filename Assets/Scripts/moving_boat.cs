﻿using UnityEngine;
using System.Collections;

public class moving_boat : MonoBehaviour {

    private catch_letter_audio scr_catch_letter_audio;
    private bool audio_flag = true;

	// Use this for initialization
	void Start () {

        scr_catch_letter_audio = GameObject.Find("catch_letter").GetComponent<catch_letter_audio>();
	}
	
	// Update is called once per frame
	void Update () {

        if (transform.position.x > 0)
            transform.Translate(new Vector3(-0.1f, 0, 0));
        else
            if (audio_flag)
                StartCoroutine(WaitOneSecond());
	}

    IEnumerator WaitOneSecond()
    {
        audio_flag = false;
        yield return new WaitForSeconds(2);

        scr_catch_letter_audio.enabled = true;
        Destroy(GameObject.Find("start_panel"));//, 1);
    }
}
