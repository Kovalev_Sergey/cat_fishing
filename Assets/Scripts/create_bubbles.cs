﻿using UnityEngine;
using System.Collections;

public class create_bubbles : MonoBehaviour
{

    public float myTimer = 6;
    private GameObject bubble;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (myTimer > 0)
            myTimer -= Time.deltaTime;
        else if (myTimer <= 0.0f)
        {
            generate_bubble();
        }
    }

    void generate_bubble()
    {
        myTimer = Random.Range(1.0f, 2.0f);

        string bubble_name = "bubble " + Random.Range(1, 4);
        bubble = (GameObject)Instantiate(Resources.Load(bubble_name));

        bubble.AddComponent<move_bubbles>();
    }
}
