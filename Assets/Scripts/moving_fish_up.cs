﻿using UnityEngine;
using System.Collections;

public class moving_fish_up : MonoBehaviour {

    private GameObject catch_letter;
    private bool correct_fish = true;
    private bool flag_audio = true;
    
    // Use this for initialization
	void Start () {

        catch_letter = GameObject.Find("catch_letter");
        GetComponent<BoxCollider2D>().enabled = false;

        transform.position = new Vector3(0, transform.position.y, transform.position.z);
        transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, -90);
	}
	
	// Update is called once per frame
	void Update () {

        if (correct_fish && transform.position.y < 4.5)
            transform.Translate(new Vector3(0, 0.2f, 0), Space.World);
        else if (catch_letter.GetComponent<UILabel>().text == GetComponent<UILabel>().text)
        {
            AudioClip correct_audio = (AudioClip)Resources.Load("correct9", typeof(AudioClip));
            AudioSource.PlayClipAtPoint(correct_audio, transform.position);

            GameObject catched_fish = GameObject.Find("catched_fish");
            catched_fish_count scr = catched_fish.GetComponent<catched_fish_count>();
            scr.count++;
            
            Destroy(gameObject);
        }
        else if (transform.position.x > -8.0)
        {
            if (flag_audio)
            {
                catch_letter.GetComponent<try_catch_letter>().enabled = true;
                flag_audio = false;
            }

            correct_fish = false;

            transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0);
            transform.Translate(new Vector3(-0.1f, -0.08f, 0));
        }
        else
            Destroy(gameObject);
	}
}
