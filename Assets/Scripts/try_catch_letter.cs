﻿using UnityEngine;
using System.Collections;

public class try_catch_letter : MonoBehaviour {

    public AudioClip try_again_a;
    public AudioClip try_again_b;
    public AudioClip try_again_c;
    public AudioClip try_again_d;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GetComponent<UILabel>().text == "a")
        {
            AudioSource.PlayClipAtPoint(try_again_a, transform.position);
        }
        else if (GetComponent<UILabel>().text == "b")
        {
            AudioSource.PlayClipAtPoint(try_again_b, transform.position);
        }
        else if (GetComponent<UILabel>().text == "c")
        {
            AudioSource.PlayClipAtPoint(try_again_c, transform.position);
        }
        else if (GetComponent<UILabel>().text == "d")
        {
            AudioSource.PlayClipAtPoint(try_again_d, transform.position);
        }

        this.enabled = false;
	}
}
