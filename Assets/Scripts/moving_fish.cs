﻿using UnityEngine;
using System.Collections;

public class moving_fish : MonoBehaviour
{
    private BoxCollider2D hook_col;
    private float speed = -0.05f;
    private double toPoint = -8.0;
    private float direction_up_down = 0;

    // Use this for initialization
    void Start()
    {
        float rnd_start_y = Random.Range(-1.5f, -2.7f);
        if (rnd_start_y > -2)
            direction_up_down = -1;
        else
            direction_up_down = 1;

        transform.position = new Vector3(7.4f, rnd_start_y, -0.01f);
        
        gameObject.AddComponent<generate_letter>();

        hook_col = GameObject.Find("hook").GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > toPoint)
        {
            transform.Translate(new Vector3(speed, direction_up_down * Mathf.Sin(transform.position.x / 450f), 0));
        }
        else
            Destroy(gameObject);
    }
    void OnCollisionEnter2D()
    {
        gameObject.AddComponent<moving_fish_up>();
        hook_col.enabled = false;
        Destroy(this);
    }
}