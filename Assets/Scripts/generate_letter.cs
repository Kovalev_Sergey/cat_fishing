﻿using UnityEngine;
using System.Collections;

public class generate_letter : MonoBehaviour {

	// Use this for initialization
	void Start () {
    
        string[] arr_letters = { "a", "b", "c", "d" };
        UILabel letter = GetComponent<UILabel>();
        int rnd = Random.Range(0, 4);
        letter.text = arr_letters[rnd];

        Destroy(this);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
