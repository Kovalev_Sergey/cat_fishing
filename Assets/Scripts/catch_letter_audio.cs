﻿using UnityEngine;
using System.Collections;

public class catch_letter_audio : MonoBehaviour {

    public AudioClip catch_a;
    public AudioClip catch_b;
    public AudioClip catch_c;
    public AudioClip catch_d;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GetComponent<UILabel>().text == "a")
        {
            AudioSource.PlayClipAtPoint(catch_a, transform.position);
        }
        else if (GetComponent<UILabel>().text == "b")
        {
            AudioSource.PlayClipAtPoint(catch_b, transform.position);
        }
        else if (GetComponent<UILabel>().text == "c")
        {
            AudioSource.PlayClipAtPoint(catch_c, transform.position);
        }
        else if (GetComponent<UILabel>().text == "d")
        {
            AudioSource.PlayClipAtPoint(catch_d, transform.position);
        }

        this.enabled = false;
	}
}
