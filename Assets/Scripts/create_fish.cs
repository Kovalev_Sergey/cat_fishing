﻿using UnityEngine;
using System.Collections;

public class create_fish : MonoBehaviour {

    public float myTimer = 6;
    private GameObject fish;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (myTimer > 0)
            myTimer -= Time.deltaTime;
        else if (myTimer <= 0.0f)
        {
            generate_fish();
        }
	}

    void generate_fish()
    {
        myTimer = Random.Range(1.0f, 1.5f);
        
        string fish_name = "fish " + Random.Range(1, 7);
        fish = (GameObject)Instantiate(Resources.Load(fish_name));
        
        fish.AddComponent<moving_fish>();
    }
}
