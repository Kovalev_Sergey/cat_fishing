﻿using UnityEngine;
using System.Collections;

public class button_catch : MonoBehaviour {

    public GameObject hook;
    public double position_y;
    public float speedUp;
    private hook_down scr;
    private bool flag = false;

	// Use this for initialization
	void Start () {
        scr = hook.GetComponent<hook_down>();
    }
	
	// Update is called once per frame
    void Update()
    {
        if (flag == true && hook.transform.position.y < position_y)
            hook.transform.Translate(new Vector3(0, speedUp, 0));//(Vector3.up, Space.World);
        else
        {
            scr.enabled = true;
            flag = false;
        }
    }

    void OnMouseDown()
    {
        scr.enabled = false;
        hook.collider2D.enabled = true;

        if (hook.transform.position.y < position_y)
        {
            Update();
            flag = true;
        }
    }
}
