﻿using UnityEngine;
using System.Collections;

public class catched_fish_count : MonoBehaviour {

    public int count = 0;
    private catch_letter_audio scr_catch_letter_audio;

    //private Object green_1 = GameObject.Find("catched_fish_1").transform.GetChild(0);
    //private Object green_2 = GameObject.Find("catched_fish_2").transform.GetChild(0);
    //private Object green_3 = GameObject.Find("catched_fish_3").transform.GetChild(0);

	// Use this for initialization
	void Start () {

        //var green_1 = GameObject.Find("catched_fish_1").transform.GetChild(0);
        //var green_2 = GameObject.Find("catched_fish_2").transform.GetChild(0);
        //var green_3 = GameObject.Find("catched_fish_3").transform.GetChild(0);

        scr_catch_letter_audio = GameObject.Find("catch_letter").GetComponent<catch_letter_audio>();
	}
	
	// Update is called once per frame
	void Update () {

        if (count == 1)
        {
            var green_1 = GameObject.Find("catched_fish_1").transform.GetChild(0);
            green_1.active = true;
        }
        if (count == 2)
        {
            var green_2 = GameObject.Find("catched_fish_2").transform.GetChild(1);
            green_2.active = true;
        }
        if (count == 3)
        {
            var green_3 = GameObject.Find("catched_fish_3").transform.GetChild(0);
            green_3.active = true;
            
            StartCoroutine(WaitTwoSecond());
        }
    }

    IEnumerator WaitTwoSecond()
    {
        count = 0;
        
        yield return new WaitForSeconds(2);

        GameObject.Find("catch_letter").AddComponent<generate_letter>();

        scr_catch_letter_audio.enabled = true;

        var green_1 = GameObject.Find("catched_fish_1").transform.GetChild(0);
        var green_2 = GameObject.Find("catched_fish_2").transform.GetChild(1);
        var green_3 = GameObject.Find("catched_fish_3").transform.GetChild(0);
        
        green_1.active = false;
        green_2.active = false;
        green_3.active = false;
    }
}
