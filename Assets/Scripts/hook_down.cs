﻿using UnityEngine;
using System.Collections;

public class hook_down : MonoBehaviour {

    public float myTimer;
    public GameObject button_catch;
    private Collider2D button_collider;

	// Use this for initialization
	void Start () {

        button_collider = button_catch.GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update () {

        if(myTimer > 0)
            myTimer -= Time.deltaTime;
        else if (myTimer <= 0.0f)
        {
            if (transform.position.y > -3.2)
            {
                button_collider.enabled = false;
                transform.Translate(new Vector3(0, -0.1f, 0));
            }
            else
                button_collider.enabled = true;
        }
	}
}
