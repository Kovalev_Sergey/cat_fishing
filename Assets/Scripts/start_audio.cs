﻿using UnityEngine;
using System.Collections;

public class start_audio : MonoBehaviour {

    public AudioClip myClip;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        AudioSource.PlayClipAtPoint(myClip, transform.position);
        Destroy(this);
	}
}
