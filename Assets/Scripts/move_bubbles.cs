﻿using UnityEngine;
using System.Collections;

public class move_bubbles : MonoBehaviour {

    private float speed = 0.1f;
    private double toPoint = 4.3;

	// Use this for initialization
	void Start () {

        float rnd_start_x = Random.Range(-5.5f, 5.5f);

        transform.position = new Vector3(rnd_start_x, -4.3f, 0.001f);
	}
	
	// Update is called once per frame
	void Update () {

        if (transform.position.y < toPoint)
        {
            transform.Translate(new Vector3(0, speed, 0));
        }
        else
            Destroy(gameObject);
	}
}
